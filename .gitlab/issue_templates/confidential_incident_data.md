/confidential 
/label ~"keep confidential"

<!-- CONFIDENTIAL ISSUES: Try to keep the primary incident public. If necessary use a vague title. Any confidential information (summary, timeline, findings) should be contained in a linked, confidential issue. -->


## Timeline

<!--
Try to capture in this section, among other events:
- Time estimation for when the errors started - typically before the incident was declared.
- When the incident was declared.
- If other teams had to be engaged, when the right Subject Matter Expert (SME) - able to effectively work on the incident mitigation - was engaged.
- When the CMOC sent first comms for this incident.
- When the incident was mitigated.
- When the incident was fully resolved.
- A link to the original PagerDuty incident page, if any.
- ...
-->

**Recent Events (available internally only):**

* [Deployments](https://nonprod-log.gitlab.net/goto/b20f7f27a257959cbc40d3b60af231ac)
* [Feature Flag Changes](https://nonprod-log.gitlab.net/goto/ef8330a5bf26e872457f7b45983c96df)
* [Infrastructure Configurations](https://nonprod-log.gitlab.net/goto/08064e2c976475f64c52924e3e9918b0)
* [GCP Events](https://log.gprd.gitlab.net/goto/c7212d51c34e012f7b7926df9a408851) (e.g. host failure)

All times UTC.

<!-- woodhouse: '`{{ .Date }}`' -->`YYYY-MM-DD`

<!-- woodhouse: '- `{{ .Time }}` - {{ .Username }} declares incident in Slack.' -->- `00:00` - ...

## Takeaways

<!--
- Highlight key takeaways from this incident. This can include:
    - Something we learned from the incident.
    - Things that were surprising or unexpected.
    - Things that went well during incident response.
-->

- ...

## Corrective Actions

<!--
- List issues that have been created as corrective actions from this incident.
- For each issue, include the following:
    - A one sentence summary of the corrective action.
    - <Bare Issue link> - Issue labeled as ~"corrective action".
- If an incident review was completed, use Lessons Learned as a guideline for creation of Corrective Actions
-->

Corrective actions should be put here as soon as an incident is mitigated, ensure that all corrective actions mentioned in the notes below are included.

- ...

----

**Note:**
In some cases we need to redact information from public view. We only do this in a limited number of documented cases. This might include the summary, timeline or any other bits of information, laid out in out [handbook page](https://about.gitlab.com/handbook/communication/#not-public). Any of this confidential data will be in a linked issue, only visible internally.
**By default, all information we can share, will be public**, in accordance to our [transparency value](https://about.gitlab.com/handbook/values/#transparency).

