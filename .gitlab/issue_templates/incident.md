/label ~incident ~"Incident::Active"
/lock

<!-- CONFIDENTIAL ISSUES: Try to keep the primary incident public. If necessary use a vague title. Any confidential information (summary, timeline, findings) should be contained in a linked, confidential issue. -->

## Incident DRI
<!-- Every incident should have a named DRI. They are responsible for keeping track of the incident and actively working to get it resolved. This person could be, but doesn't have to be the EOC-->


## Current Status

<!-- Leave a brief headline remark so that people know what's going on. It is perfectly acceptable for this to be vague while not much is known.  -->

More information will be added as we investigate the issue.
For customers believed to be affected by this incident, please subscribe to this issue or monitor our status page for further updates.

#### Summary for CMOC notice / Exec summary:

1. Customer Impact: {+ Human-friendly 1-sentence statement on impacted +}
1. Service Impact: {+ service:: labels of services impacted by this incident +}
1. Impact Duration: {+ start time UTC +} - {+ end time UTC +} ({+ duration in minutes +})
1. Root cause: {+ TBD +}


## Timeline

<!--
Try to capture in this section, among other events:
- Time estimation for when the errors started - typically before the incident was declared.
- When the incident was declared.
- If other teams had to be engaged, when the right Subject Matter Expert (SME) - able to effectively work on the incident mitigation - was engaged.
- When the CMOC sent first comms for this incident.
- When the incident was mitigated.
- When the incident was fully resolved.
- A link to the original PagerDuty incident page, if any.
- ...
-->

**Recent Events (available internally only):**

* [Deployments](https://nonprod-log.gitlab.net/goto/b20f7f27a257959cbc40d3b60af231ac)
* [Feature Flag Changes](https://nonprod-log.gitlab.net/goto/ef8330a5bf26e872457f7b45983c96df)
* [Infrastructure Configurations](https://nonprod-log.gitlab.net/goto/08064e2c976475f64c52924e3e9918b0)
* [GCP Events](https://log.gprd.gitlab.net/goto/c7212d51c34e012f7b7926df9a408851) (e.g. host failure)

All times UTC.

<!-- woodhouse: '`{{ .Date }}`' -->`YYYY-MM-DD`

<!-- woodhouse: '- `{{ .Time }}` - {{ .Username }} declares incident in Slack.' -->- `00:00` - ...

## Takeaways

<!--
- Highlight key takeaways from this incident. This can include:
    - Something we learned from the incident.
    - Things that were surprising or unexpected.
    - Things that went well during incident response.
-->

- ...

## Corrective Actions

<!--
- List issues that have been created as corrective actions from this incident.
- For each issue, include the following:
    - A one sentence summary of the corrective action.
    - <Bare Issue link> - Issue labeled as ~"corrective action".
- If an incident review was completed, use Lessons Learned as a guideline for creation of Corrective Actions
-->

Corrective actions should be put here as soon as an incident is mitigated, ensure that all corrective actions mentioned in the notes below are included.

- ...

----

**Note:**
In some cases we need to redact information from public view. We only do this in a limited number of documented cases. This might include the summary, timeline or any other bits of information, laid out in out [handbook page](https://about.gitlab.com/handbook/communication/#not-public). Any of this confidential data will be in a linked issue, only visible internally.
**By default, all information we can share, will be public**, in accordance to our [transparency value](https://about.gitlab.com/handbook/values/#transparency).

[Create a confidential issue](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/new?issuable_template=confidential_incident_data)

<!-- THE BELOW IS TO BE CONDUCTED ONCE THE ABOVE INCIDENT IS MITIGATED. -->
<br/>
<details>
<summary><i>Click to expand or collapse the Incident Review section.</i>
<br/>

# Incident Review
</summary>

<!--

The purpose of this Incident Review is to serve as a classroom to help us better understand the root causes of an incident. Treating it as a classroom allows us to create the space to let us focus on devising the mechanisms needed to prevent a similar incident from recurring in the future. A root cause can **never be a person** and this Incident Review should be written to refer to the system and the context rather than the specific actors. As placeholders for names, consider the usage of nouns like "technician", "engineer on-call", "developer", etc..

_For calculating duration of event, use the [Platform Metrics Dashboard](https://dashboards.gitlab.net/d/general-triage/general-platform-triage?orgId=1) to look at appdex and SLO violations._

-->

- [ ] Ensure that the exec summary is completed at the top of the incident issue, the timeline is updated and relevant graphs are included in the summary
- [ ] If there are any ~"corrective action" items mentioned in the notes on the incident, ensure they are listed in the "Corrective Action" section
- [ ] Fill out relevant sections below or link to the meeting review notes that cover these topics

## Customer Impact

1. **Who was impacted by this incident? (i.e. external customers, internal customers)**
    1. ...
2. **What was the customer experience during the incident? (i.e. preventing them from doing X, incorrect display of Y, ...)**
    1. ...
3. **How many customers were affected?**
    1. ...
4. **If a precise customer impact number is unknown, what is the estimated impact (number and ratio of failed requests, amount of traffic drop, ...)?**
    1. ...

## What were the root causes?

- ...

## Incident Response Analysis

1. **How was the incident detected?**
    1. ...
1. **How could detection time be improved?**
    1. ...
1. **How was the root cause diagnosed?**
    1. ...
1. **How could time to diagnosis be improved?**
    1. ...
1. **How did we reach the point where we knew how to mitigate the impact?**
    1. ...
1. **How could time to mitigation be improved?**
    1. ...
1. **What went well?**
    1. ...

## Post Incident Analysis

1. **Did we have other events in the past with the same root cause?**
    1. ...
1. **Do we have existing backlog items that would've prevented or greatly reduced the impact of this incident?**
    1. ...
1. **Was this incident triggered by a change (deployment of code or change to infrastructure)? If yes, link the issue.**
    1. ...

## What went well?

<!--
Use this section to highlight what went well during the incident. Capturing this helps understand informal
processes and expertise, and enables undocumented knowledge to be shared.

_example:_
1. We quickly discovered a recently changed feature flag through the event log which enabled fast mitigation of the impact, as well as pulling in the engineer involved to further diagnose.
2. We escalated through dev escalations, which brought in Person X. They knew that Person Y had expertise with the component in question, which enabled faster diagnosis.
3. Person Z discovered a misconfiguration in component A by using a methodology they had used previously. This method was not known by anyone else involved in the incident, and that contribution was crucial to understanding the underlying mechanism of the incident.
-->

- ...

## Guidelines

* [Blameless RCA Guideline](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/workflows/internal/root-cause-analysis.html#meeting-purpose)

## Resources

1. If the **Situation Zoom room** was utilised, recording will be automatically uploaded to [Incident room Google Drive folder](https://drive.google.com/drive/folders/1wtGTU10-sybbCv1LiHIj2AFEbxizlcks) (private)

</details>
