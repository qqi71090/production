<!--
Please review https://about.gitlab.com/handbook/engineering/infrastructure/change-management/ for the most recent information on our change plans and execution policies.
-->

# Production Change

### Change Summary

Perform OS upgrade on Gitaly nodes by rebuilding the VMs on {{ .Environment }}

{+ Please substitute the environment and add any other details relevant (e.g. which hosts will be affected if going in batches) +}

### Change Details

1. **Services Impacted**  -  ~"Service::Gitaly"
1. **Change Technician**  - <!-- woodhouse: '`@{{ .Username }}`' -->{+ DRI for the execution of this change +}
1. **Change Reviewer**    - <!-- woodhouse: '@{{ .Reviewer }}' -->{+ DRI for the review of this change +}
1. **Time tracking**      - 50 + 10 minutes for each 4 servers in the batch
1. **Downtime Component** - 5 minutes for each server in the batch (only 4 shards with downtime at a time)

## Detailed steps for the change

### Pre-Change Steps - steps to be completed before execution of the change

*Estimated Time to Complete (mins)* - 45

- [ ] Coordinate with delivery an execution time that doesn't coincide with a deployment
- [ ] Determine which shards will be included in the batch and add a list of the target shards in a comment below
- [ ] Clone and setup https://gitlab.com/gitlab-com/gl-infra/ansible-workloads/gitaly-os-upgrade
- [ ] Start the script for this batch: `bin/rebuild {{ .Environment }} 2`. When the packer image is built the runbook will present a prompt like the following: `Target base image: 'packer-gitaly-gprd-XXXX'. Create an MR to update the base images of packertest and the target modules to it`
- [ ] Prepare a MR for https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt where you set the `os_boot_image` of the appropriate file modules, plus the `file-packertest` module, to the image name from the previous step: https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/merge_requests/3450
- [ ] Merge the image update MR
- [ ] On the playbook, confirm that the MR is merged, and continue execution. The runbook will verify the image on packertest
- [ ] When prompted `Proceed to rebuild (y/n)?`, wait for maintenance window before continuing
- [ ] Set label ~"change::in-progress" on this issue

### Change Steps - steps to take to execute the change

*Estimated Time to Complete (mins)* - 5 per each 4 servers in the batch

- [ ] Confirm `Proceed to rebuild` on the playbook, and continue execution

### Post-Change Steps - steps to take to verify the change

*Estimated Time to Complete (mins)* - 5

- [ ] Notify `@release-managers` of completion so deployments can resume
- [ ] Confirm that the number of shards remaining for upgrade matches expectations
- [ ] If this is the final batch, check that all shards are running the expected version

## Rollback

### Rollback steps - steps to be taken in the event of a need to rollback this change

*Estimated Time to Complete (mins)* - 5 per server in the batch

- [ ] Create a revert MR for the https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt change
- [ ] Merge and apply the revert MR
- [ ] Run the rebuild playbook: `ansible-playbook run.yml -i inventory/gstg --limit batch2`

## Monitoring

### Key metrics to observe

<!--
  * Describe which dashboards and which specific metrics we should be monitoring related to this change using the format below.
-->

- Metric: Gitaly Per-Node Service Aggregated SLIs: Apdex, Error ratio, RPS
  - Location: https://dashboards.gitlab.net/d/gitaly-host-detail/gitaly-host-detail?orgId=1&var-PROMETHEUS_DS=Global&var-environment={{ .Environment }}&var-fqdn={{ .InstanceName }}.c.{{ .GCPProject }}.internal {{+ Repeat for each instance of the batch +}}
  - What changes to this metric should prompt a rollback: Degradation violating our SLOs _after_ the instance rebuild has been completed.

- Metic: Chef client errors
  - Location: https://prometheus.gprd.gitlab.net/graph?g0.expr=chef_client_error%7Btype%3D%22gitaly%22%7D%20%3D%3D%201&g0.tab=0&g0.stacked=0&g0.show_exemplars=0&g0.range_input=1h
  - What changes to this metric should prompt a rollback: Failures due to misconfiguration of the chef client

- Metric: git Service Error Ratio
  - Location: https://dashboards.gitlab.net/d/git-main/git-overview?orgId=1&viewPanel=1941771315&var-PROMETHEUS_DS=Global&var-environment={{ .Environment }}&var-stage=main
  - What changes to this metric should prompt a rollback: Degradation violating our SLOs, but pay attention to any degradation as the effect of a single server may not be dramatic on the overall service rate

## Summary of infrastructure changes

- [ ] Does this change introduce new compute instances? No
- [ ] Does this change re-size any existing compute instances? No
- [ ] Does this change introduce any additional usage of tooling like Elastic Search, CDNs, Cloudflare, etc? No

<!--
  * If you answer yes to any of the items in this checklist, summarize below.
-->
{+Summary of the above+}

## Change Reviewer checklist

<!--
To be filled out by the reviewer.
-->
~C4 ~C3 ~C2 ~C1:
- [ ] The **scheduled day and time** of execution of the change is appropriate.
- [ ] The [change plan](#detailed-steps-for-the-change) is technically accurate.
- [ ] The change plan includes **estimated timing values** based on previous testing.
- [ ] The change plan includes a viable [rollback plan](#rollback).
- [ ] The specified [metrics/monitoring dashboards](#key-metrics-to-observe) provide sufficient visibility for the change.

~C2 ~C1:
- [ ] The complexity of the plan is appropriate for the corresponding risk of the change. (i.e. the plan contains clear details).
- [ ] The change plan includes success measures for all steps/milestones during the execution.
- [ ] The change adequately minimizes risk within the environment/service.
- [ ] The performance implications of executing the change are well-understood and documented.
- [ ] The specified metrics/monitoring dashboards provide sufficient visibility for the change.
        - If not, is it possible (or necessary) to make changes to observability platforms for added visibility?
- [ ] The change has a primary and secondary SRE with knowledge of the details available during the change window.

## Change Technician checklist

<!--
To find out who is on-call, in #production channel run: /chatops run oncall production.
-->

- [ ] This issue has a criticality label (e.g. ~C1, ~C2, ~C3, ~C4) and a change-type label (e.g. ~"change::unscheduled", ~"change::scheduled") based on the [Change Management Criticalities](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/#change-criticalities).
- [ ] This issue has the change technician as the assignee.
- [ ] Pre-Change, Change, Post-Change, and Rollback steps and have been filled out and reviewed.
- [ ] This Change Issue is linked to the appropriate Issue and/or Epic
- [ ] Necessary approvals have been completed based on the [Change Management Workflow](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/#change-request-workflows).
- [ ] Change has been tested in staging and results noted in a comment on this issue.
- [ ] A dry-run has been conducted and results noted in a comment on this issue.
- [ ] SRE on-call has been informed prior to change being rolled out. (In #production channel, mention `@sre-oncall` and this issue and await their acknowledgement.)
- [ ] Release managers have been informed (If needed! Cases include DB change) prior to change being rolled out. (In #production channel, mention `@release-managers` and this issue and await their acknowledgment.)
- [ ] There are currently no [active incidents](https://gitlab.com/gitlab-com/gl-infra/production/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Incident%3A%3AActive).
